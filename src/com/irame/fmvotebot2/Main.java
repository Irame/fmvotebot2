package com.irame.fmvotebot2;

import com.irame.fmvotebot2.Data.DBWrapper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Felix on 04.06.2014.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("GUI/MainWindow.fxml"));
        primaryStage.setTitle("FMVoteBot2");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

        DBWrapper db = DBWrapper.getInstance();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
