package com.irame.fmvotebot2.Data;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.util.Date;

/**
 * Created by Felix on 07.06.2014.
 */
public class DBTest {
    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {
        DBWrapper db = DBWrapper.getInstance();
        UserDAO userDAO = new UserDAOImpl(db.getConnection());
        VoteUrlDAO voteUrlDAO = new VoteUrlDAOImpl(db.getConnection());

        User user = new User("Irame1", "12345");
        userDAO.insertUser(user);

        VoteUrl voteUrl = new VoteUrl("VoteURL1", new URL("http://frostmourne.eu"), Time.valueOf("12:00:00"), new Date(0), user.getId());
        VoteUrl voteUrl2 = new VoteUrl("VoteURL2", new URL("http://frostmourne2.eu"), Time.valueOf("12:00:00"), new Date(0), user.getId());
        VoteUrl voteUrl3 = new VoteUrl("VoteURL3", new URL("http://frostmourne3.eu"), Time.valueOf("12:00:00"), new Date(0), 3);


        voteUrlDAO.insertVoteUrl(voteUrl);
        voteUrlDAO.insertVoteUrl(voteUrl2);
        voteUrlDAO.insertVoteUrl(voteUrl3);

        voteUrlDAO.findByUser(user).forEach(System.out::println);
    }
}
