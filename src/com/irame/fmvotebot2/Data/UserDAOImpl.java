package com.irame.fmvotebot2.Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Felix on 06.06.2014.
 */
public class UserDAOImpl implements UserDAO {
    private Connection connection;
    private String findByIdStatement;
    private String findAllStatement;
    private String insertUserStatement;
    private String updateUserStatement;
    private String deleteUserStatement;

    public UserDAOImpl(Connection connection) {
        this.connection = connection;

        findByIdStatement = "SELECT * FROM user WHERE ID = %d;";
        findAllStatement = "SELECT * FROM user;";
        insertUserStatement = "INSERT INTO user (NAME, PASS, WEBSITE) VALUES ('%s', '%s', %s);";
        updateUserStatement = "UPDATE user SET NAME = '%s', PASS = '%s', WEBSITE = %s WHERE ID = %d;";
        deleteUserStatement = "DELETE FROM user WHERE ID = %d;";
    }


    @Override
    public User findById(int Id) {
        User user = null;
        try {
            String statement = String.format(findByIdStatement, Id);
            System.out.println(statement);
            ResultSet resultSet = connection.createStatement().executeQuery(statement);
            if (resultSet.next()) {
                user = new User(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        LinkedList<User> users = new LinkedList<>();
        try {
            System.out.println(findAllStatement);
            ResultSet resultSet = connection.createStatement().executeQuery(findAllStatement);
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4)
                ));
                System.out.println(resultSet.getInt(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public boolean insertUser(User user) {
        boolean result = true;
        try {
            String websiteId = user.getWebsiteId() == 0 ? "NULL" : Integer.toString(user.getWebsiteId());
            String statement = String.format(insertUserStatement, user.getUserName(), user.getUserPassword(), websiteId);
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT last_insert_rowid();");
            if (resultSet.next()) {
                user.setId(resultSet.getInt(1));
            } else {
                result = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateUser(User user) {
        boolean result = false;
        try {
            String websiteId = user.getWebsiteId() == 0 ? "NULL" : Integer.toString(user.getWebsiteId());
            String statement = String.format(updateUserStatement, user.getUserName(), user.getUserPassword(), websiteId, user.getId());
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deleteUser(User user) {
        return deleteUser(user.getId());
    }

    @Override
    public boolean deleteUser(int id) {
        boolean result = false;
        try {
            String statement = String.format(deleteUserStatement, id);
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
