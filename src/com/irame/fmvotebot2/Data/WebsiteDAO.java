package com.irame.fmvotebot2.Data;

import java.util.List;

/**
 * Created by Felix on 06.06.2014.
 */
public interface WebsiteDAO {
    public List<Website> findAll();

    public Website findById(int Id);

    public Website findByUser(User user);

    public Website findByUser(int Id);

    public boolean insertWebsite(Website website);

    public boolean updateWebsite(Website website);

    public boolean deleteWebsite(Website website);

    public boolean deleteWebsite(int Id);
}
