package com.irame.fmvotebot2.Data;

import java.util.List;

/**
 * Created by Felix on 06.06.2014.
 */
public interface UserDAO {
    public User findById(int Id);

    public List<User> findAll();

    public boolean insertUser(User user);

    public boolean updateUser(User user);

    public boolean deleteUser(User user);

    public boolean deleteUser(int id);
}
