package com.irame.fmvotebot2.Data;

/**
 * Created by Felix on 06.06.2014.
 */
public class User {
    private int Id;
    private String userName;
    private String userPassword;
    private int websiteId;

    public User(String userName, String userPassword) {
        this(0, userName, userPassword, 0);
    }

    public User(String userName, String userPassword, int websiteId) {
        this(0, userName, userPassword, websiteId);
    }

    public User(int id, String userName, String userPassword, int websiteId) {
        Id = id;
        this.userName = userName;
        this.userPassword = userPassword;
        this.websiteId = websiteId;
    }

    public int getId() {
        return Id;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public int getWebsiteId() {
        return websiteId;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public void setWebsiteId(int websiteId) {
        this.websiteId = websiteId;
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", userName='" + userName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", websiteId=" + websiteId +
                '}';
    }
}
