package com.irame.fmvotebot2.Data;

import java.net.URL;

/**
 * Created by Felix on 06.06.2014.
 */
public class Website {
    private int Id;
    private String websiteName;
    private URL url;
    private String loginPostPattern;

    public Website(String websiteName, URL url, String loginPostPattern) {
        this(0, websiteName, url, loginPostPattern);
    }

    public Website(int id, String websiteName, URL url, String loginPostPattern) {
        Id = id;
        this.websiteName = websiteName;
        this.url = url;
        this.loginPostPattern = loginPostPattern;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getLoginPostPattern() {
        return loginPostPattern;
    }

    public void setLoginPostPattern(String loginPostPattern) {
        this.loginPostPattern = loginPostPattern;
    }

    @Override
    public String toString() {
        return "Website{" +
                "Id=" + Id +
                ", websiteName='" + websiteName + '\'' +
                ", url=" + url +
                ", loginPostPattern='" + loginPostPattern + '\'' +
                '}';
    }
}
