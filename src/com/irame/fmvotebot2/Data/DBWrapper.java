package com.irame.fmvotebot2.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Felix on 06.06.2014.
 */
public class DBWrapper {

    private Connection connection;
    private static DBWrapper dbInstance = new DBWrapper();

    private DBWrapper() {

        if (connection == null) {

            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:data.db");
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        try {
            Statement statement = connection.createStatement();

            //enable foreign key support
            statement.execute("PRAGMA foreign_keys = ON");

            //create website table
            statement.execute("" +
                    "CREATE TABLE IF NOT EXISTS website (\n" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "NAME VARCHAR(50) NOT NULL,\n" +
                    "URL VARCHAR(1000) NOT NULL,\n" +
                    "LOGIN_PATTERN TEXT NOT NULL\n" +
                    ")");

            // create user table
            statement.execute("" +
                    "CREATE TABLE IF NOT EXISTS user (\n" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "NAME VARCHAR(50) NOT NULL,\n" +
                    "PASS VARCHAR(50) NOT NULL,\n" +
                    "WEBSITE INTEGER DEFAULT NULL REFERENCES website(ID) ON DELETE SET DEFAULT\n" +
                    ")");

            //create vote_url table
            statement.execute("" +
                    "CREATE TABLE IF NOT EXISTS vote_url (\n" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "NAME VARCHAR(50) NOT NULL,\n" +
                    "URL VARCHAR(1000) NOT NULL,\n" +
                    "DELAY INTEGER NOT NULL,\n" +
                    "LAST_VOTE INTEGER DEFAULT NULL,\n" +
                    "USER INTEGER REFERENCES user(ID) ON DELETE CASCADE\n" +
                    ")");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static DBWrapper getInstance() {
        return dbInstance;
    }

    public Connection getConnection() {
        return connection;
    }

    /**
     * {@see java.lang.Object
     */
    @Override
    public void finalize() throws Throwable {
        if (connection != null) {
            connection.close();
        }
        super.finalize();
    }
}
