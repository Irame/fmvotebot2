package com.irame.fmvotebot2.Data;

import java.util.List;

/**
 * Created by Felix on 06.06.2014.
 */
public interface VoteUrlDAO {
    public VoteUrl findById(int Id);

    public List<VoteUrl> findByUser(User user);

    public List<VoteUrl> findByUser(int Id);

    public boolean insertVoteUrl(VoteUrl voteUrl);

    public boolean updateVoteUrl(VoteUrl voteUrl);

    public boolean deleteVoteUrl(VoteUrl voteUrl);

    public boolean deleteVoteUrl(int id);
}
