package com.irame.fmvotebot2.Data;

import java.net.URL;
import java.sql.Time;
import java.util.Date;

/**
 * Created by Felix on 06.06.2014.
 */
public class VoteUrl {
    private int Id;
    private String name;
    private URL url;
    private Time voteDelay;
    private Date lastVote;
    private int user;

    public VoteUrl(String name, URL url, Time voteDelay, int user) {
        this(0, name, url, voteDelay, new Date(0), user);
    }

    public VoteUrl(String name, URL url, Time voteDelay, Date lastVote, int user) {
        this(0, name, url, voteDelay, lastVote, user);
    }

    public VoteUrl(int id, String name, URL url, Time voteDelay, Date lastVote, int user) {
        Id = id;
        this.name = name;
        this.url = url;
        this.voteDelay = voteDelay;
        this.lastVote = lastVote;
        this.user = user;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public Time getVoteDelay() {
        return voteDelay;
    }

    public void setVoteDelay(Time voteDelay) {
        this.voteDelay = voteDelay;
    }

    public Date getLastVote() {
        return lastVote;
    }

    public void setLastVote(Date lastVote) {
        this.lastVote = lastVote;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "VoteUrl{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", url=" + url +
                ", voteDelay=" + voteDelay +
                ", lastVote=" + lastVote +
                ", user=" + user +
                '}';
    }
}
