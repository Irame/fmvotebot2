package com.irame.fmvotebot2.Data;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Felix on 07.06.2014.
 */
public class VoteUrlDAOImpl implements VoteUrlDAO {
    private Connection connection;

    private String findByIdStatement;
    private String findByUserStatement;
    private String insertVoteUrlStatement;
    private String updateVoteUrlStatement;
    private String deleteVoteUrlStatement;

    public VoteUrlDAOImpl(Connection connection) {
        this.connection = connection;

        findByIdStatement = "SELECT * FROM vote_url WHERE ID = %d;";
        findByUserStatement = "SELECT * FROM vote_url WHERE USER = %d;";
        insertVoteUrlStatement = "INSERT INTO vote_url (NAME, URL, DELAY, LAST_VOTE, USER) VALUES ('%s', '%s', %d, %d, %d);";
        updateVoteUrlStatement = "UPDATE vote_url SET NAME = '%s', URL = '%s', DELAY = %d, LAST_VOTE = %d, USER = %d WHERE ID = %d;";
        deleteVoteUrlStatement = "DELETE FROM vote_url WHERE ID = %d;";
    }

    @Override
    public VoteUrl findById(int Id) {
        VoteUrl voteUrl = null;
        try {
            String statement = String.format(findByIdStatement, Id);
            System.out.println(statement);
            ResultSet resultSet = connection.createStatement().executeQuery(statement);
            if (resultSet.next()) {
                voteUrl = new VoteUrl(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getURL(3),
                        resultSet.getTime(4),
                        resultSet.getDate(5),
                        resultSet.getInt(6)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            voteUrl = null;
        }
        return voteUrl;
    }

    @Override
    public List<VoteUrl> findByUser(User user) {
        return findByUser(user == null ? 0 : user.getId());
    }

    @Override
    public List<VoteUrl> findByUser(int Id) {
        LinkedList<VoteUrl> voteUrls = new LinkedList<>();
        if (Id != 0) {
            try {
                String statement = String.format(findByUserStatement, Id);
                System.out.println(statement);
                ResultSet resultSet = connection.createStatement().executeQuery(statement);
                while (resultSet.next()) {
                    voteUrls.add(new VoteUrl(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            new URL(resultSet.getString(3)),
                            resultSet.getTime(4),
                            resultSet.getDate(5),
                            resultSet.getInt(6)
                    ));
                }
            } catch (SQLException | MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return voteUrls;
    }

    @Override
    public boolean insertVoteUrl(VoteUrl voteUrl) {
        boolean result = true;
        try {
            String statement = String.format(insertVoteUrlStatement, voteUrl.getName(), voteUrl.getUrl(), voteUrl.getVoteDelay().getTime(), voteUrl.getLastVote().getTime(), voteUrl.getUser());
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT last_insert_rowid();");
            if (resultSet.next()) {
                voteUrl.setId(resultSet.getInt(1));
            } else {
                result = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateVoteUrl(VoteUrl voteUrl) {
        boolean result = false;
        try {
            String statement = String.format(updateVoteUrlStatement, voteUrl.getName(), voteUrl.getUrl(), voteUrl.getVoteDelay().getTime(), voteUrl.getLastVote().getTime(), voteUrl.getUser(), voteUrl.getId());
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deleteVoteUrl(VoteUrl voteUrl) {
        return deleteVoteUrl(voteUrl.getId());
    }

    @Override
    public boolean deleteVoteUrl(int Id) {
        boolean result = false;
        try {
            String statement = String.format(deleteVoteUrlStatement, Id);
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
