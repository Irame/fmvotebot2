package com.irame.fmvotebot2.Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Felix on 07.06.2014.
 */
public class WebsiteDAOImpl implements WebsiteDAO {
    private Connection connection;

    private String findAllStatement;
    private String findByIdStatement;
    private String findByUserStatement;
    private String insertWebsiteStatement;
    private String updateWebsiteStatement;
    private String deleteWebsiteStatement;

    public WebsiteDAOImpl(Connection connection) {
        this.connection = connection;

        findAllStatement = "SELECT * FROM website;";
        findByIdStatement = "SELECT * FROM website WHERE ID = %d;";
        findByUserStatement = "SELECT w.ID, w.NAME, w.URL, w.LOGIN_PATTERN FROM website AS w\n" +
                "JOIN user AS u ON w.ID = u.WEBSITE\n" +
                "WHERE u.ID = %d;";
        insertWebsiteStatement = "INSERT INTO website (NAME, URL, LOGIN_PATTERN) VALUES ('%s', '%s', '%s');";
        updateWebsiteStatement = "UPDATE website SET NAME = '%s', URL = '%s', LOGIN_PATTERN = '%s' WHERE ID = %d;";
        deleteWebsiteStatement = "DELETE FROM website WHERE ID = %d;";
    }

    @Override
    public List<Website> findAll() {
        LinkedList<Website> websites = new LinkedList<>();
        try {
            System.out.println(findAllStatement);
            ResultSet resultSet = connection.createStatement().executeQuery(findAllStatement);
            while (resultSet.next()) {
                websites.add(new Website(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getURL(3),
                        resultSet.getString(4)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return websites;
    }

    @Override
    public Website findById(int Id) {
        Website website = null;
        try {
            String statement = String.format(findByIdStatement, Id);
            System.out.println(statement);
            ResultSet resultSet = connection.createStatement().executeQuery(statement);
            if (resultSet.next()) {
                website = new Website(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getURL(3),
                        resultSet.getString(4)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            website = null;
        }
        return website;
    }

    @Override
    public Website findByUser(User user) {
        return findByUser(user.getId());
    }

    @Override
    public Website findByUser(int Id) {
        Website website = null;
        try {
            String statement = String.format(findByUserStatement, Id);
            System.out.println(statement);
            ResultSet resultSet = connection.createStatement().executeQuery(statement);
            if (resultSet.next()) {
                website = new Website(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getURL(3),
                        resultSet.getString(4)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            website = null;
        }
        return website;
    }

    @Override
    public boolean insertWebsite(Website website) {
        boolean result = true;
        try {
            String statement = String.format(insertWebsiteStatement, website.getWebsiteName(), website.getUrl(), website.getLoginPostPattern());
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT last_insert_rowid();");
            if (resultSet.next()) {
                website.setId(resultSet.getInt(1));
            } else {
                result = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateWebsite(Website website) {
        boolean result = false;
        try {
            String statement = String.format(updateWebsiteStatement, website.getWebsiteName(), website.getUrl(), website.getLoginPostPattern(), website.getId());
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deleteWebsite(Website website) {
        return deleteWebsite(website.getId());
    }

    @Override
    public boolean deleteWebsite(int Id) {
        boolean result = false;
        try {
            String statement = String.format(deleteWebsiteStatement, Id);
            System.out.println(statement);
            result = connection.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
