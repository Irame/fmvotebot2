package com.irame.fmvotebot2.GUI;

import com.irame.fmvotebot2.Data.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.TimeZone;

/**
 * Created by Felix on 06.06.2014.
 */
public class SettingsController implements Initializable {

    public ListView<User> accList_settings_listView;
    public ListView<VoteUrl> voteList_settings_listView;
    public PasswordField accPassword_settings_passwordField;
    public TextField accName_settings_textField;
    public ComboBox<Website> accWebsite_settings_comboBox;
    public Button accWebsiteManage_settings_button;
    public TextField voteName_settings_textField;
    public TextField voteUrl_settings_textField;
    public TextField voteFreq_settings_textField;


    private DBWrapper db;
    private UserDAO userDAO;
    private VoteUrlDAO voteUrlDAO;
    private WebsiteDAO websiteDAO;

    private ObservableList<User> userObservableList;
    private ObservableList<VoteUrl> voteUrlObservableList;
    private ObservableList<Website> websiteObservableList;

    private SimpleDateFormat delayFormat;

    private Stage websiteManagerStage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        db = DBWrapper.getInstance();
        userDAO = new UserDAOImpl(db.getConnection());
        voteUrlDAO = new VoteUrlDAOImpl(db.getConnection());
        websiteDAO = new WebsiteDAOImpl(db.getConnection());

        userObservableList = FXCollections.observableArrayList(userDAO.findAll());
        accList_settings_listView.setItems(userObservableList);

        voteUrlObservableList = FXCollections.observableArrayList();
        voteList_settings_listView.setItems(voteUrlObservableList);

        websiteObservableList = FXCollections.observableArrayList(websiteDAO.findAll());
        accWebsite_settings_comboBox.setItems(websiteObservableList);

        accList_settings_listView.getSelectionModel().selectedItemProperty().addListener((observableValue, userOld, userNew) -> {
            if (userNew != null) {
                accName_settings_textField.setText(userNew.getUserName());
                accPassword_settings_passwordField.setText(userNew.getUserPassword());
                for (Website website : websiteObservableList) {
                    if (website.getId() == userNew.getWebsiteId()) {
                        accWebsite_settings_comboBox.setValue(website);
                        break;
                    }
                }
            } else {
                accName_settings_textField.setText("");
                accPassword_settings_passwordField.setText("");
            }
            updateVoteUrlList();
        });

        voteList_settings_listView.getSelectionModel().selectedItemProperty().addListener((observableValue, voteUrlOld, voteUrlNew) -> {
            if (voteUrlNew != null) {
                voteName_settings_textField.setText(voteUrlNew.getName());
                voteUrl_settings_textField.setText(voteUrlNew.getUrl().toString());
                voteFreq_settings_textField.setText(delayFormat.format(voteUrlNew.getVoteDelay()));
            } else {
                voteName_settings_textField.setText("");
                voteUrl_settings_textField.setText("");
                voteFreq_settings_textField.setText("0");
            }
        });

        websiteObservableList.addListener((ListChangeListener<Website>) change -> {
            System.out.println("websiteObservableList changed => updateUserList");
            updateUserList();
        });

        delayFormat = new SimpleDateFormat("HH:mm:ss");
        delayFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public void accAddButtonClicked(ActionEvent actionEvent) {
        String name = accName_settings_textField.getText();
        String password = accPassword_settings_passwordField.getText();

        Website website = (accWebsite_settings_comboBox.getSelectionModel().getSelectedItem());
        int websiteId = website == null ? 0 : website.getId();

        name = name.trim().equals("") ?
                "<Unbekannt>" :
                name.trim();

        User newUser = new User(name, password, websiteId);

        userDAO.insertUser(newUser);
        userObservableList.add(newUser);
        accList_settings_listView.getSelectionModel().select(newUser);
    }

    public void accRemoveButtonClicked(ActionEvent actionEvent) {
        User curUser = userObservableList.remove(accList_settings_listView.getSelectionModel().getSelectedIndex());
        if (curUser != null) {
            userDAO.deleteUser(curUser);
        }
    }

    public void voteAddButtonClicked(ActionEvent actionEvent) {
        User curUser = accList_settings_listView.getSelectionModel().getSelectedItem();
        if (curUser != null) {
            String name = voteName_settings_textField.getText();
            String urlString = voteUrl_settings_textField.getText();

            Time delay;
            try {
                delay = new Time(delayFormat.parse(voteFreq_settings_textField.getText()).getTime());
            } catch (ParseException e) {
                System.err.println(">> Wrong formatted Time!\n" + e.getMessage());
                delay = new Time(0);
            }

            URL url, defaultUrl = null;
            try {
                defaultUrl = new URL("http://");
                url = new URL(urlString);
            } catch (MalformedURLException e) {
                System.err.println(">> Wrong formatted URL!\n" + e.getMessage());
                url = defaultUrl;
            }

            name = name.trim().equals("") ?
                    "<Unbekannt>" :
                    name.trim();

            VoteUrl newVoteUrl = new VoteUrl(name, url, delay, curUser.getId());
            voteUrlDAO.insertVoteUrl(newVoteUrl);
            voteUrlObservableList.add(newVoteUrl);
            voteList_settings_listView.getSelectionModel().select(newVoteUrl);
        }
    }

    public void voteRemoveButtonClicked(ActionEvent actionEvent) {
        VoteUrl voteUrl = voteUrlObservableList.remove(voteList_settings_listView.getSelectionModel().getSelectedIndex());
        if (voteUrl != null) {
            voteUrlDAO.deleteVoteUrl(voteUrl);
        }
    }

    public void accWebsiteManageButtonClicked(ActionEvent actionEvent) throws IOException {
        if (websiteManagerStage == null) {
            websiteManagerStage = new Stage();
            FXMLLoader settingsFxmlLoader = new FXMLLoader(getClass().getResource("WebsiteManagerWindow.fxml"));
            Parent settingsRoot = settingsFxmlLoader.load();
            websiteManagerStage.setTitle("Website Manager");
            websiteManagerStage.setScene(new Scene(settingsRoot, 400, 400));
            websiteManagerStage.setMinWidth(300);
            websiteManagerStage.setMinHeight(300);
            ((WebsiteManagerController) settingsFxmlLoader.getController()).setWebsiteList(websiteObservableList);

        }
        websiteManagerStage.show();
    }

    public void accSaveButtonClicked(ActionEvent actionEvent) {
        User curUser = accList_settings_listView.getSelectionModel().getSelectedItem();
        if (curUser != null) {
            String name = accName_settings_textField.getText();
            String password = accPassword_settings_passwordField.getText();
            Website website = accWebsite_settings_comboBox.getSelectionModel().getSelectedItem();

            if (!name.trim().equals(""))
                curUser.setUserName(name.trim());

            curUser.setUserPassword(password);

            if (website != null) {
                curUser.setWebsiteId(website.getId());
            }

            userDAO.updateUser(curUser);
            forceListRefreshOn(accList_settings_listView);
        }
    }

    public void voteSaveButtonClicked(ActionEvent actionEvent) {
        VoteUrl curVoteUrl = voteList_settings_listView.getSelectionModel().getSelectedItem();
        User curUser = accList_settings_listView.getSelectionModel().getSelectedItem();
        if (curUser != null && curVoteUrl != null) {
            String name = voteName_settings_textField.getText();
            String urlString = voteUrl_settings_textField.getText();

            Time delay;
            try {
                delay = new Time(delayFormat.parse(voteFreq_settings_textField.getText()).getTime());
                curVoteUrl.setVoteDelay(delay);
            } catch (ParseException e) {
                System.err.println(">> Wrong formatted Time!\n" + e.getMessage());
            }

            URL url;
            try {
                url = new URL(urlString);
                curVoteUrl.setUrl(url);
            } catch (MalformedURLException e) {
                System.err.println(">> Wrong formatted URL!\n" + e.getMessage());
            }

            if (!name.trim().equals(""))
                curVoteUrl.setName(name.trim());

            voteUrlDAO.updateVoteUrl(curVoteUrl);
            forceListRefreshOn(voteList_settings_listView);
        }
    }

    private void updateUserList() {
        userObservableList.clear();
        userObservableList.addAll(userDAO.findAll());
    }

    private void updateVoteUrlList() {
        voteUrlObservableList.clear();
        voteUrlObservableList.addAll(voteUrlDAO.findByUser(accList_settings_listView.getSelectionModel().getSelectedItem()));
    }

    private <T> void forceListRefreshOn(ListView<T> listView) {
        int selected = listView.getSelectionModel().getSelectedIndex();
        ObservableList<T> items = listView.<T>getItems();
        listView.<T>setItems(null);
        listView.<T>setItems(items);
        listView.getSelectionModel().select(selected);
    }
}
