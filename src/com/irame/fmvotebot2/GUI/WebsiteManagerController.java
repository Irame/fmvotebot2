package com.irame.fmvotebot2.GUI;

import com.irame.fmvotebot2.Data.DBWrapper;
import com.irame.fmvotebot2.Data.Website;
import com.irame.fmvotebot2.Data.WebsiteDAO;
import com.irame.fmvotebot2.Data.WebsiteDAOImpl;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Felix on 08.06.2014.
 */
public class WebsiteManagerController implements Initializable {
    public ListView<Website> websiteList_mgr_listView;
    public TextField websiteName_mgr_textField;
    public TextField websiteUrl_mgr_textField;
    public TextArea loginPattern_mgr_textArea;

    private DBWrapper db;
    private WebsiteDAO websiteDAO;

    private ObservableList<Website> websiteObservableList;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        db = DBWrapper.getInstance();
        websiteDAO = new WebsiteDAOImpl(db.getConnection());

        websiteList_mgr_listView.getSelectionModel().selectedItemProperty().addListener((observableValue, websiteOld, websiteNew) -> {
            if (websiteNew != null) {
                websiteName_mgr_textField.setText(websiteNew.getWebsiteName());
                websiteUrl_mgr_textField.setText(websiteNew.getUrl().toString());
                loginPattern_mgr_textArea.setText(websiteNew.getLoginPostPattern());
            }
        });
    }

    public void saveClicked(ActionEvent actionEvent) {
        Website curWebsite = websiteList_mgr_listView.getSelectionModel().getSelectedItem();
        if (curWebsite != null) {
            String name = websiteName_mgr_textField.getText();
            String urlString = websiteUrl_mgr_textField.getText();
            String loginPattern = loginPattern_mgr_textArea.getText();

            URL url;
            try {
                url = new URL(urlString);
                curWebsite.setUrl(url);
            } catch (MalformedURLException e) {
                System.err.println(">> Wrong formatted URL!\n" + e.getMessage());
            }

            if (!name.trim().equals(""))
                curWebsite.setWebsiteName(name.trim());

            curWebsite.setLoginPostPattern(loginPattern);

            websiteDAO.updateWebsite(curWebsite);
            forceListRefreshOn(websiteList_mgr_listView);
        }
    }

    public void addClicked(ActionEvent actionEvent) {
        String name = websiteName_mgr_textField.getText();
        String urlString = websiteUrl_mgr_textField.getText();
        String loginPattern = loginPattern_mgr_textArea.getText();

        URL url, defaultUrl = null;
        try {
            defaultUrl = new URL("http://");
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            System.err.println(">> Wrong formatted URL!\n" + e.getMessage());
            url = defaultUrl;
        }

        name = name.trim().equals("") ?
                "<Unbekannt>" :
                name.trim();

        Website newWebsite = new Website(name, url, loginPattern);

        websiteDAO.insertWebsite(newWebsite);
        websiteObservableList.add(newWebsite);
        websiteList_mgr_listView.getSelectionModel().select(newWebsite);
    }

    public void removeClicked(ActionEvent actionEvent) {
        int selectedIndex = websiteList_mgr_listView.getSelectionModel().getSelectedIndex();
        Website website = websiteObservableList.get(selectedIndex);
        if (website != null) {
            websiteDAO.deleteWebsite(website);
        }
        websiteObservableList.remove(selectedIndex);
    }

    private <T> void forceListRefreshOn(ListView<T> listView) {
        int selected = listView.getSelectionModel().getSelectedIndex();
        ObservableList<T> items = listView.<T>getItems();
        listView.<T>setItems(null);
        listView.<T>setItems(items);
        listView.getSelectionModel().select(selected);
    }

    public void setWebsiteList(ObservableList<Website> websiteObservableList) {
        this.websiteObservableList = websiteObservableList;
        websiteList_mgr_listView.setItems(websiteObservableList);
    }
}
