package com.irame.fmvotebot2.GUI;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Felix on 21.05.2014.
 */
public class MainController {
    public TextArea log_textArea;
    public MenuItem close_menuItem;
    public MenuBar topBar_menuBar;
    public Menu file_menu;
    public Menu settings_menu;
    public MenuItem account_menuItem;
    public MenuItem votes_menuItem;

    private Stage settingsStage;

    public void settingsClicked(ActionEvent actionEvent) throws IOException {
        if (settingsStage == null) {
            settingsStage = new Stage();
            FXMLLoader settingsFxmlLoader = new FXMLLoader(getClass().getResource("SettingsWindow.fxml"));
            Parent settingsRoot = settingsFxmlLoader.load();
            settingsStage.setTitle("Settings");
            settingsStage.setScene(new Scene(settingsRoot, 600, 400));
            settingsStage.setMinWidth(400);
            settingsStage.setMinHeight(300);
        }
        settingsStage.show();
    }

    public void closeClicked(ActionEvent actionEvent) {
        ((Stage) log_textArea.getScene().getWindow()).close();
    }

    public void startClicked(ActionEvent actionEvent) {

    }

    public void stopClicked(ActionEvent actionEvent) {

    }
}
